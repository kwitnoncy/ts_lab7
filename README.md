**Guess Number**

Simple game that I have to make to pass my network class.

---

## Manual

1. Run server_main.py
2. Run two instances od client_main.py (REMEMBER TO CHANGE IP ADDRESES)
3. Than each client sends to serer number. Server takes those numbers and ramdomly picks number (which is between received ones). That number is to geuss by clients.

## Datagrams

1. Operation:
	- X001 -> Set up -> setting up connection
	- X010 -> Answer -> check Data
	- X011 -> Finish -> game is finished
	- X100 -> Result -> check Solution
	- X101 -> Set L  -> sets limits of number to guess (send clients to server)
	- X110 -> Set L1 -> sets upper limit (sends server to clients)
	- X111 -> Set L2 -> sets lower limit (sends server to clients)
	- 1XXX -> ACK    -> acknowledge flag. If 1 client/serevr is acknowledging getting message, 0 if not.
2. Solution:
	- 1000 -> You won
	- 0100 -> You lost
	- 0010 -> Nobody won, keep playing
	- 0001 -> Tie, two clients guessed number with same number of attempts
3. ID:
	- XXXX - 4 bits that indicate unique identifier
4. Data:
	- XXXX XXXXXXXX - 12 bits for data. Here are number placed.
5. Examples (each 8 bits are seperated wich space):
	- |Operation|Solution|ID|DATA|Comments
	- |0001|0000|0000|000000000000|First message (SET flag)
	- |1001|0000|0000|000000000000|And server's respond (ACK for SET from client)
	- |0101|0000|1001|101011100101|Client wich ID equals to 9 wants to set a limit of 2789
	- |0010|0000|0101|100001010110|Clinnt wich ID equals to 5 answers wich number 2134
	- |0011|0000|0111|111111111111|Clinnt wich ID equals to 7 wants to finish game
	- |0100|1000|1110|000000000000|Server send information to clients with equals to 14 that he won.
	- |1110|0000|0110|101010101010|Client wich ID equals to 6 acknowledges that upper limit is 2730


