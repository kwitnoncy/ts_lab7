from socket import *
from random import randint
import sys


class Server_class():
    def __init__(self):
        print(gethostbyname(gethostname()))
        self.running = True
        self.L1 = 0
        self.L2 = 0
        self.ID1 = randint(1, 15)
        self.ID2 = randint(1, 15)
        while self.ID1 is self.ID2:
            self.ID2 = randint(1, 15)

        print("ID1: ", self.ID1)
        print("ID2: ", self.ID2)

        self.socket1 = socket(AF_INET, SOCK_DGRAM)
        self.socket1.bind(('', 8000))

        #   nawiazanie polaczneie od klientow
        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)
        mess1 = bytearray(data1)
        mess2 = bytearray(data2)
        mess1 = self.after_r(mess1)#
        mess2 = self.after_r(mess2)#
        self.add_ack(mess1)
        self.add_ack(mess2)

        #   potwierdzenie nawiazania polacznie
        mess1 = self.before_send(mess1)#
        mess2 = self.before_send(mess2)#
        self.socket1.sendto(mess1, addr1)
        self.socket1.sendto(mess2, addr2)

        #   dodanie do wiadomosci ID
        mess1 = self.after_r(mess1)#
        mess2 = self.after_r(mess2)#
        mess1 = bytearray(mess1)
        mess2 = bytearray(mess2)
        self.add_ID(mess1, self.ID1)
        self.add_ID(mess2, self.ID2)
        mess1[0] = 0xa1
        mess2[0] = 0xa1

        #   wyslanie wiadomosci z ID
        mess1 = self.before_send(mess1)#
        mess2 = self.before_send(mess2)#
        self.socket1.sendto(mess1, addr1)
        self.socket1.sendto(mess2, addr2)

        # odebranie ACK z ID
        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)

        data1 = self.after_r(data1)#
        data2 = self.after_r(data2)#

        while addr1 == addr2:
            data2, addr2 = self.socket1.recvfrom(1024)
            data2 = self.after_r(data2)#
            print("*")

        if data1[0] < 0xA8 or data2[0] < 0xA:
            print("brak ACK")
            sys.exit(1)

        self.run()

    def run(self):
        # odebranie od klientow zakresu losowania liczby
        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)

        data1 = bytearray(data1)
        data2 = bytearray(data2)
        data1 = self.after_r(data1)#
        data2 = self.after_r(data2)#

        num11 = data1[3] & 0b00001111
        num12 = data1[4] & 0b11111111

        num21 = data2[3] & 0b00001111
        num22 = data2[4] & 0b11111111

        num1 = num11 * 0x100 + num12
        num2 = num21 * 0x100 + num22
        ACK_L_mess1 = data1
        ACK_L_mess2 = data2

        self.add_ack(ACK_L_mess1)
        self.add_ack(ACK_L_mess2)

        ACK_L_mess1 = self.before_send(ACK_L_mess1)#
        ACK_L_mess2 = self.before_send(ACK_L_mess2)#

        # ACK odebrania od klientow zakresu losowania liczby
        self.socket1.sendto(ACK_L_mess1, addr1)
        self.socket1.sendto(ACK_L_mess2, addr2)

        # poszeregowanie zakresu
        if num1 > num2:
            self.L1 = num1
            self.L2 = num2
        elif num1 == num2:
           self.L1 = num1
           self.L2 = num1
        else:
            self.L1 = num2
            self.L2 = num1

        # przesłanie clientom górnego zakresu
        self.del_ack(data1)
        self.del_ack(data2)
        mess_set_L1_c1 = bytearray(data1)
        mess_set_L1_c2 = bytearray(data2)
        self.add_num(mess_set_L1_c1, self.L1)
        self.add_num(mess_set_L1_c2, self.L1)
        mess_set_L1_c1[0] = 0xA6
        mess_set_L1_c2[0] = 0xA6
        mess_set_L1_c1 = self.before_send(mess_set_L1_c1)#
        mess_set_L1_c2 = self.before_send(mess_set_L1_c2)#
        self.socket1.sendto(mess_set_L1_c1, addr1)
        self.socket1.sendto(mess_set_L1_c2, addr2)

        # ACK przesłania clientom górnego zakresu
        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)
        data1 = self.after_r(data1)#
        data2 = self.after_r(data2)#

        if (data1[0] < 0xA8 or data2[0] < 0xA8):
            print("brak ACK")
            sys.exit(1)

        # przesłanie clientom dolego zakresu
        data1 = bytearray(data1)
        data2 = bytearray(data2)
        self.del_ack(data1)
        self.del_ack(data2)
        mess_set_L2_c1 = bytearray(data1)
        mess_set_L2_c2 = bytearray(data2)
        self.add_num(mess_set_L2_c1, self.L2)
        self.add_num(mess_set_L2_c2, self.L2)
        mess_set_L2_c1[0] = 0xA7
        mess_set_L2_c2[0] = 0xA7
        mess_set_L2_c1 = self.before_send(mess_set_L2_c1)#
        mess_set_L2_c2 = self.before_send(mess_set_L2_c2)#
        self.socket1.sendto(mess_set_L2_c1, addr1)
        self.socket1.sendto(mess_set_L2_c2, addr2)

        # ACK przesłania clientom gódolnego zakresu
        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)
        data1 = self.after_r(data1)#
        data2 = self.after_r(data2)#

        if data1[0] < 0xA8 or data2[0] < 0xA8:
            print("brak ACK")
            sys.exit(1)
        else:
            print("good")

        # wylosowanie liczby
        self.num_to_guess = randint(self.L2, self.L1)
        print(self.num_to_guess, " num to guess")

        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x00)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x00)
        Mess.append((0x0 * 0x10) + 0x00)

        while self.running:
            # odebranie od klientów "strzałów"
            data1, addr1 = self.socket1.recvfrom(1024)
            data2, addr2 = self.socket1.recvfrom(1024)
            data1 = self.after_r(data1)#
            data2 = self.after_r(data2)#

            data1 = bytearray(data1)
            data2 = bytearray(data2)
            numer1 = self.get_num(data1)
            numer2 = self.get_num(data2)

            while numer1 == 0:
                print("czekanie 1")
                data1, addr1 = self.socket1.recvfrom(1024)
                data1 = bytearray(data1)
                data1 = self.after_r(data1)#
                numer1 = self.get_num(data1)
            while numer2 == 0:
                print("czekanie 2")
                data2, addr2 = self.socket1.recvfrom(1024)
                data2= bytearray(data2)
                data2 = self.after_r(data2)#
                numer2 = self.get_num(data2)

            # pokazanie liczb podanych przez klientów oraz wysłanie podwierdzenie otrzymania "strałów"
            print("n1", numer1, " adres: ", addr1)
            print("n2", numer2, " adres: ", addr2)
            self.add_ack(data1)
            self.add_ack(data2)
            data1 = self.before_send(data1)#
            data2 = self.before_send(data2)#
            self.socket1.sendto(data1, addr1)
            self.socket1.sendto(data2, addr2)
            data1 = self.after_r(data1)
            data2 = self.after_r(data2)

            # sprawdzenie czy któryś z clientów wygrał oraz przygotowanie wiadomości to wysładnia z wynikami
            if numer1 == self.num_to_guess:
                if numer1 == numer2:
                    data1[0] = 0xA4
                    data1[1] = 0xB1
                    data1[3] = 0xD0
                    data1[4] = 0x00
                    ###############
                    data2[0] = 0xA4
                    data2[1] = 0xB1
                    data2[3] = 0xD0
                    data2[4] = 0x00
                    ###############
                else:
                    data1[0] = 0xA4
                    data1[1] = 0xB8
                    data1[3] = 0xD0
                    data1[4] = 0x00
                    ###############
                    data2[0] = 0xA4
                    data2[1] = 0xB4
                    data2[3] = 0xD0
                    data2[4] = 0x00
                    ###############

                data1 = self.before_send(data1)  #
                data2 = self.before_send(data2)  #
                self.socket1.sendto(data1, addr1)
                self.socket1.sendto(data2, addr2)

                self.running = False
            elif numer2 == self.num_to_guess:
                data1[0] = 0xA4
                data1[1] = 0xB4
                data1[3] = 0xD0
                data1[4] = 0x00
                ###############
                data2[0] = 0xA4
                data2[1] = 0xB8
                data2[3] = 0xD0
                data2[4] = 0x00
                ###############

                data1 = self.before_send(data1)  #
                data2 = self.before_send(data2)  #
                self.socket1.sendto(data1, addr1)
                self.socket1.sendto(data2, addr2)
                self.running = False
            else:
                data1[0] = 0xA4
                data1[1] = 0xB2
                data1[3] = 0xD0
                data1[4] = 0x00
                ###############
                data2[0] = 0xA4
                data2[1] = 0xB2
                data2[3] = 0xD0
                data2[4] = 0x00
                ###############

                data1 = self.before_send(data1)  #
                data2 = self.before_send(data2)  #
                self.socket1.sendto(data1, addr1)
                self.socket1.sendto(data2, addr2)
                self.running = True

        data1, addr1 = self.socket1.recvfrom(1024)
        data2, addr2 = self.socket1.recvfrom(1024)

        data1 = bytearray(data1)
        data2 = bytearray(data2)
        data1 = self.after_r(data1)#
        data2 = self.after_r(data2)#
        numer1 = self.get_num(data1)
        numer2 = self.get_num(data2)

        while numer1 == 0:
            data1, addr1 = self.socket1.recvfrom(1024)
            data1 = self.after_r(data1)#
            data1 = bytearray(data1)
            numer1 = self.get_num(data1)
        while numer2 == 0:
            data2, addr2 = self.socket1.recvfrom(1024)
            data2 = self.after_r(data2)#
            data2 = bytearray(data2)
            numer2 = self.get_num(data2)

        self.add_ack(data1)
        self.add_ack(data2)
        data1 = self.before_send(data1)  #
        data2 = self.before_send(data2)  #
        self.socket1.sendto(data1, addr1)
        self.socket1.sendto(data2, addr2)

        print("wyslano ACK Koniec pracy serwera")

    # dodanie flagi ACK do wiadmosci
    def add_ack(self, x):
        x[0] = x[0] | 0x8

    # usuniecie flagi ACK
    def del_ack(self, x):
        x[0] = x[0] & 0b11110111

    # dodanie ID od wiadomosci
    def add_ID(self, x, id):
        x[2] = x[2] | id

    # dodanie numeru do pola dancyh w wiadomosci
    def add_num(self, mess, num):
        num1 = num & 0b111100000000
        num1 = num1 >> 8
        num2 = num & 0b000011111111

        mess[3] = mess[3] & 0b11110000
        mess[4] = mess[4] & 0b00000000
        mess[3] = mess[3] | num1
        mess[4] = mess[4] | num2

    #odczytanie wiadomosci z pola danych wiadomosci
    def get_num(self, mess):
        num1 = 0
        num2 = 0
        num1 = mess[3] & 0b00001111
        num2 = mess[4]

        num1 = num1 << 8
        return num1 | num2

    # "skompresowanie" wiadomosci
    def before_send(self, mess):
        d1 = int(mess[0] & 0b00001111)
        d2 = int(mess[1] & 0b00001111)
        d3 = int(mess[2] & 0b00001111)
        d4 = int(mess[3] & 0b00001111)
        d5 = int(mess[4] & 0b11111111)

        to_ret = bytearray()
        to_ret.append((d1 * 0x10) + d2)
        to_ret.append((d3 * 0x10) + d4)
        to_ret.append(d5)
        return to_ret

    # "dekompresja" wiaomosci
    def after_r(self, mess):
        d1 = int(mess[0] & 0b11110000)
        d2 = int(mess[0] & 0b00001111)
        d3 = int(mess[1] & 0b11110000)
        d4 = int(mess[1] & 0b00001111)
        d5 = int(mess[2])
        d1 = d1 >> 4
        d3 = d3 >> 4

        to_ret = bytearray()
        to_ret.append((0xA * 0x10) + d1)
        to_ret.append((0xB * 0x10) + d2)
        to_ret.append((0xC * 0x10) + d3)
        to_ret.append((0xD * 0x10) + d4)
        to_ret.append((0x0 * 0x10) + d5)

        return to_ret
