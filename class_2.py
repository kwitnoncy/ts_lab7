from socket import *
import sys
import os

class Client():
    def __init__(self):
        # przypogowanie wiadomosci
        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x01)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x00)
        Mess.append((0x0 * 0x10) + 0x00)

        self.L1 = -1
        self.L2 = -1

        # przygotowanie gniazda
        self.socket = socket(AF_INET, SOCK_DGRAM)

        # wyslanie wiadomosci inicjalizacyjnej i odebranie ACK
        Mess = Mess = self.before_send(Mess)#
        self.socket.sendto(Mess, ('127.0.0.1', 8000))
        data, addr = self.socket.recvfrom(1024)
        data = bytearray(data)
        data = self.after_r(data)#
        self.address_to_send = addr

        if(data[0] < 0xA8):
            print("1brak ACK")
            print(data)
            sys.exit(1)

        # odebranie ID i wyslanie ACK
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)#
        data = bytearray(data)
        self.ID = int(data[2] & 0b00001111)
        print("ID: ", self.ID)
        self.add_ack(data)
        data = self.before_send(data)#
        self.socket.sendto(data, addr)

        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x05)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x00)
        Mess.append((0x0 * 0x10) + 0x00)

        pick_num_l = True

        while pick_num_l:
            flag = False
            send = False
            os.system("cls")
            print("***Gra w zgadywanie liczby***")
            print("*****Prosze podac liczbe*****")
            print("********L -> <1, 4095>*******")

            # pobranie od uzytkownika wartosci (zakres)
            x = input()

            # sprawdzenie czy podana wartość jest liczbą
            try:
                x = int(x)
                flag = True
            except ValueError:
                try:
                    x = int(x, 2)
                    flag = True
                except ValueError:
                    try:
                        x = int(x, 16)
                        flag = True
                    except ValueError:
                        print("miala byc liczba")

            if flag:
                if x >= 1 and x <= 4095:
                    pick_num_l = False
                else:
                    print("zly zakres")

        # spreparwoanie waidomosci i przeslanie wartosci do serwara oraz odebranie ACK
        self.add_ID(Mess)
        self.add_num(Mess, x)
        Mess = self.before_send(Mess)#
        self.socket.sendto(Mess, self.address_to_send)

        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)#
        print(data)

        if (data[0] < 0xA8):
            print("2brak ACK")
            sys.exit(1)

        # odebranie od serwera wartosci L1, wyswietlenie go w konsoli, odebranie ACK
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)#
        self.L1 = self.get_num(data)
        print("L1 ", self.L1)

        data = bytearray(data)
        self.add_ack(data)
        data = self.before_send(data)#
        self.socket.sendto(data, addr)
        
        # odebranie od serwera wartosci L2, wyswietlenie go w konsoli, odebranie ACK
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)#
        self.L2 = self.get_num(data)
        print("L2 ", self.L2)

        data = bytearray(data)
        self.add_ack(data)
        data = self.before_send(data)#
        self.socket.sendto(data, addr)

        self.address_to_send = addr

        self.run()

    def run(self):
        send = False
        running = True
        # przypogowanie wiadomosci
        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x02)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x00)
        Mess.append((0x0 * 0x10) + 0x00)
        self.add_ID(Mess)

        while running:
            flag = False
            send = False
            os.system("cls")
            print("***Gra w zgadywanie liczby***")
            print("*****Prosze podac liczbe*****")
            print("********L -> <", self.L2, ", ", self.L1, ">*******")
            
            # pobranie od uzytkownika wartosci ("strzał")
            x = input()

            # sprawdzenie czy podana wartość jest liczbą
            try:
                x = int(x)
                flag = True
            except ValueError:
                try:
                    x = int(x, 2)
                    flag = True
                except ValueError:
                    try:
                        x = int(x, 16)
                        flag = True
                    except ValueError:
                        print("miala byc liczba")

            if flag:
                if x >= self.L2 and x <= self.L1:
                    # wowalnie metody send_n_ack(self, x)
                    self.send_n_ack(x)
                    send = True
                else:
                    print("zly zakres")

            if send:
                # wowalnie metody get_result_n_ack(self)
                result = self.get_result_n_ack()

                # sprawdzenie stanu gry i wyswietlenie odpowiednniego komunikatu na kosnoli
                if result == 8:
                    os.system("cls")
                    print("***Gra w zgadywanie liczby***")
                    print("*******Brawo wygrales********")
                    running = False
                elif result == 4:
                    os.system("cls")
                    print("***Gra w zgadywanie liczby***")
                    print("*****Sprobuj jeszcze raz*****")
                    running = False
                elif result == 2:
                    print("gra dalej")
                elif result == 1:
                    os.system("cls")
                    print("***Gra w zgadywanie liczby***")
                    print("**********  REMIS  **********")
                    running = False
                else:
                    print("result error")
                    running = False

        print("\n")
        # przypogowanie wiadomosci
        # zakończenie gry
        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x03)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x0F)
        Mess.append((0xF * 0x10) + 0x0F)

        # wyslanie wiadomości o zakończeniu fryi odebranie ACK
        self.add_ID(Mess)
        Mess = self.before_send(Mess)
        self.socket.sendto(Mess, self.address_to_send)
        print("Wysylanie zerwania polaczenia")
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)

        if (data[0] < 0xA8):
            print("brak ACK")
            sys.exit(1)

        # wyswietlenie końcowego komunikatu 
        print("Potwierdzenie zakonczenia polaczenia")
        print("*Zapraszam do ponownej gry*")

    # wysłanie wiadomości ze "strzałem" (wartość x) do serwera i odebranie ACK
    def send_n_ack(self, x):
        Mess = bytearray()
        Mess.append((0xA * 0x10) + 0x02)
        Mess.append((0xB * 0x10) + 0x00)
        Mess.append((0xC * 0x10) + 0x00)
        Mess.append((0xD * 0x10) + 0x00)
        Mess.append((0x0 * 0x10) + 0x00)

        self.add_ID(Mess)
        self.add_num(Mess, x)
        print("wyslano numer: ", x)
        Mess = self.before_send(Mess)#
        self.socket.sendto(Mess, self.address_to_send)
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)#

        if (data[0] < 0xA8):
            print("3brak ACK")
            sys.exit(1)

    # odebranie od serwera stanu rozgrywki i wysłanie ACK
    def get_result_n_ack(self):
        data, addr = self.socket.recvfrom(1024)
        data = self.after_r(data)
        data = bytearray(data)
        result = data[1] & 0b00001111
        self.add_ack(data)
        data = self.before_send(data)#
        self.socket.sendto(data, addr)
        return result

    # dodanie flagi ACk do wiadomości
    def add_ack(self, x):
        x[0] = x[0] | 0x8

    # usunięcie flagi ACk z wiadomości
    def add_ID(self, x):
        x[2] = x[2] | self.ID

    # dodanie wartości num do pola danych wiadomości mess
    def add_num(self, mess, num):
        num1 = num & 0b111100000000
        num1 = num1 >> 8
        num2 = num & 0b000011111111

        mess[3] = mess[3] & 0b11110000
        mess[4] = mess[4] & 0b00000000
        mess[3] = mess[3] | num1
        mess[4] = mess[4] | num2

    # pobranie wartości z pola danych wiadomości mess
    def get_num(self, mess):
        num1 = 0
        num2 = 0
        num1 = mess[3] & 0b00001111
        num2 = mess[4]

        num1 = num1 << 8
        return num1 | num2

    # "kompresja" wiadomości mess
    def before_send(self, mess):
        d1 = int(mess[0] & 0b00001111)
        d2 = int(mess[1] & 0b00001111)
        d3 = int(mess[2] & 0b00001111)
        d4 = int(mess[3] & 0b00001111)
        d5 = int(mess[4] & 0b11111111)

        to_ret = bytearray()
        to_ret.append((d1 * 0x10) + d2)
        to_ret.append((d3 * 0x10) + d4)
        to_ret.append(d5)
        print(bin(d1), bin(d2), bin(d3), bin(d4), bin(d5))
        print("get: ",mess, " ret: ", to_ret)
        return to_ret

    # "dekompresja" wiadomości mess
    def after_r(self, mess):
        d1 = int(mess[0] & 0b11110000)
        d2 = int(mess[0] & 0b00001111)
        d3 = int(mess[1] & 0b11110000)
        d4 = int(mess[1] & 0b00001111)
        d5 = int(mess[2])
        d1 = d1 >> 4
        d3 = d3 >> 4

        to_ret = bytearray()
        to_ret.append((0xA * 0x10) + d1)
        to_ret.append((0xB * 0x10) + d2)
        to_ret.append((0xC * 0x10) + d3)
        to_ret.append((0xD * 0x10) + d4)
        to_ret.append((0x0 * 0x10) + d5)

        return to_ret
